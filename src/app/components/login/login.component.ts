import { Component, OnInit } from '@angular/core';
import { UserLoginModel } from 'src/app/model/user/UserLoginModel';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  tempUser: UserLoginModel = new UserLoginModel();
  constructor(private authService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
  }

  login() {
    const loginUser = new UserLoginModel();

    try {
      loginUser.setUsername(this.tempUser.username);
      loginUser.setPassword(this.tempUser.password);
    } catch (e) {
      this.alertService.error(e.message);
      return;
    }

    this.authService.login(loginUser);
  }
}
