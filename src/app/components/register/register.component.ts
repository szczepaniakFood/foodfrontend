import { Component, OnInit } from '@angular/core';
import { UserRegistrationModel } from 'src/app/model/user/UserRegistrationModel';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  tempUser: UserRegistrationModel = new UserRegistrationModel();
  constructor(private userService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
  }

  register() {
    const newUser = new UserRegistrationModel();

    try {
      newUser.setUsername(this.tempUser.username);
      newUser.setPassword(this.tempUser.password);
    } catch (e) {
      this.alertService.error(e.message);
      return;
    }

    this.userService.register(newUser);
  }
}
