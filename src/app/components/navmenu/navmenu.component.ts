import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.sass']
})
export class NavmenuComponent implements OnInit {
  $isAuthenticated: boolean;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.isAuthenticated.subscribe(state => this.$isAuthenticated = state);
  }

  logout() {
    this.authService.logout();
  }
}
