import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { AlertService } from './alert.service';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserRegistrationModel } from '../model/user/UserRegistrationModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private alert: AlertService, private router: Router) { }

private authenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(localStorage.getItem('username') !== null);
 isAuthenticated: Observable<boolean> = this.authenticated.asObservable();

 register(user: UserRegistrationModel) {
  const userData = {
    username: user.username,
    password: user.password
  };
  this.http.post('api/auth/register', {}, { params: userData })
    .subscribe(
      data => {
        this.alert.success('Account has been created succesfully . Enter using your login and password.', true);
        this.router.navigate(['/login']);
      },
      error => {
        this.alert.error('user already exist');
      }
    );
}

login(user: UserRegistrationModel) {
  const userData = {
    username: user.username,
    password: user.password
  };
  this.http.post('api/auth/login', {}, { params: userData })
    .subscribe(
      data => {
        localStorage.setItem('username', user.username);
        this.authenticated.next(true);
        this.router.navigate(['/']);
      },
      error => {
        this.alert.error('Wrong login or password.');
      }
    );
}

logout() {
  this.http.post('api/auth/logout', {}).subscribe(
    data => {
      localStorage.clear();
      this.authenticated.next(false);
      this.router.navigate(['/']);
  });
}
}


